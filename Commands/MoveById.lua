function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move unit with given unit id",
		parameterDefs = {
			{ 
				name = "unitToMove",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "positionToMove",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local unit = parameter.unitToMove;
	local pos = parameter.positionToMove;

		if (pos:Distance(Vec3(Spring.GetUnitPosition(unit))) < 150)  then
		return SUCCESS;
	else
		Spring.GiveOrderToUnit(unit, CMD.MOVE, {pos.x, pos.y, pos.z}, {});
		return RUNNING;
	end
end

function Reset(self)
	return self
end
