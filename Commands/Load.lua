function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Order transporter unit to load another unit",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "toLoad",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local transport = parameter.transporter;
	local toLoad = parameter.toLoad;

	local transportingUnits = Spring.GetUnitIsTransporting(transport);

	if (transportingUnits ~= nil and #transportingUnits == 0) then
		Spring.GiveOrderToUnit(transport, CMD.LOAD_UNITS, {toLoad}, {});
		return RUNNING;
	else
		return SUCCESS;
	end
end

function Reset(self)
	return self
end
