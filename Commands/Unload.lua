function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Order transporter unit to unload another unit on given position",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "positionToUnload",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local transport = parameter.transporter;
	local pos = parameter.positionToUnload;

	local transportingUnits = Spring.GetUnitIsTransporting(transport);

	if (#transportingUnits == 0) then
		return SUCCESS;
	else
		Spring.GiveOrderToUnit(transport, CMD.UNLOAD_UNIT, {pos.x, pos.y, pos.z}, {});
		return RUNNING;
	end
end

function Reset(self)
	return self
end
