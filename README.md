# Nota AS

Repository for Nota game (http://nota.machys.net/) AI development. This AI will be developed during a Human-like Artificial Agents masters course at Faculty of Mathematics and Physics at Charles university Prague. For more informations about the course, go check out its page at https://gamedev.cuni.cz/study/courses-history/courses-2018-2019/human-like-artificial-agents-summer-201819/. 

CaptureHill Behavior icon was drawn for me by my friend Jakub Eremias.