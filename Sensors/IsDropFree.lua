local sensorInfo = {
	name = "IsDropFree",
	desc = "Can unit be safely dropped on given position",
	author = "JanHolan",
	date = "2019-05-14",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(pos)

	local freeSpotRadius = 35;
	local unitsInCylinder = Spring.GetUnitsInCylinder(pos.x, pos.z, freeSpotRadius);
	return #unitsInCylinder == 1;
end