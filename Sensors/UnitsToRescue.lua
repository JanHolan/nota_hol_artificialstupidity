local sensorInfo = {
	name = "UnitsToRescue",
	desc = "Return all rescuable units in given area",
	author = "JanHolan",
	date = "2019-05-14",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()

return function(area)
	local toRescue = {};
	local friendlyUnits = Spring.GetUnitsInCylinder(area.center.x, area.center.z, area.radius, myTeamID);

	for i=1, #friendlyUnits do
		local unit = friendlyUnits[i]
		local unitDef = Spring.GetUnitDefID(unit);
		local name = UnitDefs[unitDef].name;

		if (name == "armbox" or name == "armbull" or name == "armham" or name == "armmllt" or name == "armmllt2" or name =="armrock") then
			toRescue[#toRescue + 1] = unit;
		end
	end
	return toRescue;
end