local sensorInfo = {
	name = "Get map hills",
	desc = "Return sampled points with maximal height on map",
	author = "JanHolan",
	date = "2019-07-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description Sample map by 128 steps and find all higheest points
return function()
	local minHeight, maxHeight = Spring.GetGroundExtremes();
	local xTiles = 32;
	local zTiles = 8;
	local step = 128;
	local hills = {}


	for x = 0, xTiles do
		for z = 0, zTiles do
			local xPos = x * step;
			local zPos = z * step;

			local positionHeight = Spring.GetGroundHeight(xPos, zPos);
			if (positionHeight == maxHeight) then
				hills[#hills + 1] = Vec3(xPos, positionHeight, zPos);
			end
		end
	end

	return hills;
end