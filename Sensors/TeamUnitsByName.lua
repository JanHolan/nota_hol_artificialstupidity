local sensorInfo = {
	name = "TeamUnitsByName",
	desc = "Return units of given type in whole team",
	author = "JanHolan",
	date = "2019-05-14",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local myTeamID = Spring.GetMyTeamID()

-- @description return energy resource info
-- @argument unitName [string] unique unit name
return function(unitName)
	local unitDefID = UnitDefNames[unitName].id
	return Spring.GetTeamUnitsByDefs(myTeamID, unitDefID);
end